const getters = {
    token: state => state.user.token,
    loginStatus: state => state.user.loginStatus,
    avatar: state => state.user.avatar,
    userInfo: state => state.user.userInfo,
  }
  export default getters