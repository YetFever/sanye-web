import { setStore, getStore } from "@/utils/auth"
import { login, logout, register, retrievePassword } from '@/api/user'

const state = {
    name: '',
    avatar: getStore({
        name: 'avatar'
    }) || '',
    token: getStore({
        name: 'token'
    }) || '',
    loginStatus: getStore({
        name: 'loginStatus'
    }) || '',
    userInfo: getStore({
        name: 'userInfo'
    }) || ''
}

const mutations = {
    SET_TOKEN: (state, token) => {
        state.token = token
        setStore({
            name: 'token',
            content: token,
            type: 'session'
        })
    },
    SET_LOGINSTATUS: (state, loginStatus) => {
        state.loginStatus = loginStatus
        setStore({
            name: 'loginStatus',
            content: loginStatus,
            type: 'session'
        })
    },
    SET_AVATAR: (state, avatar) => {
        state.avatar = avatar
        setStore({
            name: 'avatar',
            content: avatar,
            type: 'session'
        })
    },
    SET_USERINFO: (state, userInfo) => {
        state.userInfo = userInfo;
        setStore({
            name: 'userInfo',
            content: userInfo,
            type: 'session'
        })
    }
}

const actions = {
    // user login 
    login({ commit }, userInfo) {
        return new Promise((resolve, reject) => {
            const { email, password } = userInfo;
            login({ email, password }).then(res => {
                if (res.code == 200) {
                    commit('SET_TOKEN', res.data.access_token)
                    commit("SET_LOGINSTATUS", true)
                    commit("SET_AVATAR", 'http://foglake.sanye666.top/images/default-avatar.jpg')
                    commit('SET_USERINFO', res.data.userInfo)
                    resolve(res.msg)
                } else {
                    reject(res.msg)
                }
            })
        })
    },
    // user logout 
    logout({ commit }) {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': `Bearer ${state.token}`
            }
            logout(headers).then(res => {
                if (res.code == 200) {
                    commit('SET_TOKEN', '')
                    commit("SET_LOGINSTATUS", false)
                    commit("SET_AVATAR", '')
                    commit('SET_USERINFO', '')
                    resolve(res.msg)
                } else {
                    reject(res.msg)
                }
            })
        })
    },
    register({ commit }, userInfo) {
        return new Promise((resolve, reject) => {
            const { name, email, code, password, password_confirmation } = userInfo;
            register({ name, email, code, password, password_confirmation }).then(res => {
                if (res.code == 200) {
                    commit('SET_TOKEN', res.data.access_token)
                    commit("SET_LOGINSTATUS", true)
                    commit("SET_AVATAR", 'http://foglake.sanye666.top/images/default-avatar.jpg')
                    commit('SET_USERINFO', res.data.userInfo)
                    resolve(res.msg)
                } else {
                    reject(res.msg)
                }
            })
        })
    },
    retrievePassword({ commit }, formData) {
        return new Promise((resolve, reject) => {
            retrievePassword(formData).then(res => {
                if (res.code == 200) {
                    commit('SET_TOKEN', res.data.access_token);
                    commit('SET_LOGINSTATUS', true);
                    commit("SET_AVATAR", 'http://foglake.sanye666.top/images/default-avatar.jpg');
                    commit('SET_USERINFO', res.data.userInfo);
                    resolve('ok')
                } else {
                    reject(res.msg)
                }
            })
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
