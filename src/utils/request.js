import axios from 'axios'
import { Message } from 'element-ui'
import Qs from 'qs'
import getSign from '@/utils/sign'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 10000, // request timeout
  transformRequest: [function (data) {
    data = Qs.stringify(data)
    return data
  }]
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    let timeStramp = Math.round(new Date().getTime() / 1000).toString();
    let sign = getSign(timeStramp)
    config.data = {
      ...config.data,
      timeStramp,
      sign
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 200) {
      Message({
        message: res.msg || 'Error',
        type: 'error',
        duration: 2 * 1000
      })
      return Promise.reject(new Error(res.msg || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log(error) // for debug
    let message = error.msg === 'Network Error'? '网络出错了#%&￥%@#':error.msg;
    Message({
      message: message,
      type: 'error',
      duration: 2 * 1000
    })
    return Promise.reject(error)
  }
)

export default service