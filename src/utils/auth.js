/**
 * Created by Yet on 19/8/18.
 */

import { validatenull } from '@/utils/validate'
import { Cookies } from 'js-cookie'
/**
 * 存储localStorage
 */
export const setStore = (params) => {
  const {
    name,
    content,
    type
  } = params
  const obj = {
    dataType: typeof (content),
    content: content,
    type: type,
    datetime: new Date().getTime()
  }
  if (type) window.sessionStorage.setItem(name, JSON.stringify(obj))
  else window.localStorage.setItem(name, JSON.stringify(obj))
}
/**
 * 获取localStorage
 */
export const getStore = (params) => {
  const {
    name
  } = params
  let obj = {}
  let content
  obj = window.localStorage.getItem(name)
  if (validatenull(obj)) obj = window.sessionStorage.getItem(name)
  if (validatenull(obj)) return
  obj = JSON.parse(obj)
  if (obj.dataType === 'string') {
    content = obj.content
  } else if (obj.dataType === 'number') {
    content = Number(obj.content)
  } else if (obj.dataType === 'boolean') {
    /* eslint-disable */
        content = eval(obj.content)
    } else if (obj.dataType === 'object') {
        content = obj.content
    }
    return content
}
/**
 * 删除localStorage
 */
export const removeStore = params => {
    const {
        name
    } = params
    window.localStorage.removeItem(name)
    window.sessionStorage.removeItem(name)
}

/**
 * 获取全部localStorage
 */
export const getAllStore = (params) => {
    const list = []
    const {
        type
    } = params
    for (let i = 1; i <= window.sessionStorage.length; i++) {
        if (type) {
            list.push({
                name: window.sessionStorage.key(i),
                content: getStore({
                    name: window.sessionStorage.key(i),
                    type: 'session'
                })
            })
        } else {
            list.push(getStore({
                name: window.localStorage.key(i),
                content: getStore({
                    name: window.localStorage.key(i)
                })
            }))
        }
    }

    return list
}

/**
 * 清空全部localStorage
 */
export const clearStore = (params) => {
    const {
        type
    } = params
    if (type) {
        window.sessionStorage.clear()
        return
    }
    window.localStorage.clear()
}

/**
 * 存储cookie
 */
export const setCookie = (params) => {
    const {
        key,
        value,
        expireDate
    } = params
    debugger
    if(expireDate){
        return Cookies.set(key, value, expireDate)
    }else{
        return Cookies.set(key, value)
    }
}

/**
 * 读取cookie
 */
export const getCookie = (params) =>{
    const {
        key
    } = params
    return Cookies.get(key)
}

/**
 * 删除cookie
 */
export const removeCookie = (params) =>{
    const{
        key
    } = params
    return Cookies.remove(key)
}


