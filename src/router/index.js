import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
    {
        path: '/',
        name: 'index',
        component: Layout,
        redirect: 'homepage',
        children: [
            {
                path: 'homepage',
                name: 'Homepage',
                component: () => import('@/views/homepage/index'),
            },
            {
                path: 'library',
                name: 'Library',
                component: () => import('@/views/library/index'),
            },
            {
                path: 'chapter',
                name: 'Chapter',
                component: () => import('@/views/chapter/index'),
            },
            {
                path: 'shelf',
                name: 'shelf',
                component: () => import('@/views/shelf/index'),
            },
            {
                path: 'search',
                name: 'Search',
                component: () => import('@/views/search/index')
            }
        ]
    },
    {
        path: '/read',
        name: 'Read',
        component: () => import('@/views/read/index'),
    },
    {
        path: '/getpassword',
        name: 'getpassword',
        component: () => import('@/views/password/index')
    },
    {
        path: '/redirect',
        name: 'redirect',
        component: () => import('@/views/redirect/index'),
    },
    {
        path: '/404',
        name: '404',
        component: () => import('@/views/404'),
    },
    { path: '*', redirect: '/404' }
]

const createRouter = () => new Router({
    mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
})

const router = createRouter()

export default router