import request from '@/utils/request'

// 首页推荐
export function getIndexList(data){
    return request({
        url:'/api/books/index',
        method:'post',
        data
    })
}

// 最近更新
export function getUpdateList(data){
    return request({
        url:'/api/books/updateList',
        method:'post',
        data
    })
}

// 完本列表
export function getOverList(data){
    return request({
        url:'/api/books/overList',
        method:'post',
        data
    })
}

// 分类推荐
export function getTypeList(data){
    return request({
        url:'/api/books/typeList',
        method:'post',
        data
    })
}

// 点击排行榜
export function getClickList(data){
    return request({
        url:'/api/books/clickList',
        method:'post',
        data
    })
}

// 搜索
export function getSearch(data){
    return request({
        url:'/api/books/search',
        method:'post',
        data
    })
}
