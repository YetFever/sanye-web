import request from '@/utils/request'

// 获取书架
export function bookShelfList(headers){
    return request({
        url:'/api/user/bookShelfList',
        method:'post',
        headers
    })
}

// 添加书籍
export function addBookShelf(data,headers){
    return request({
        url:'/api/user/addBookShelf',
        method:'post',
        data,
        headers
    })
}

// 删除书籍
export function delBookShelf(data,headers){
    return request({
        url:'/api/user/delBookShelf',
        method:'post',
        data,
        headers
    })
}

// 更新书架
export function saveBookShelf(data,headers){
    return request({
        url:'/api/user/saveBookShelf',
        method:'post',
        data,
        headers
    })
}