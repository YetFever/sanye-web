import request from '@/utils/request'

// 列表页
export function getSearchList(data){
    return request({
        url:'/api/books/bookList',
        method:'post',
        data
    })
}