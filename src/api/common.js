import request from '@/utils/request'


// 获取小说分类
export function getBookType(data){
    return request({
        url:'/api/common/getBookType',
        method:'post',
        data
    })
}

// 发送验证码
export function sendMailCode(data){
    return request({
        url:'/api/common/sendMailCode',
        method:'post',
        data
    })
}

// 验证验证码
export function checkMailCode(data){
    return request({
        url:'/api/common/checkMailCode',
        method:'post',
        data
    })
}