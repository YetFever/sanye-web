import request from '@/utils/request'

// 阅读页
export function getRead(data){
    return request({
        url:'/api/books/readContent',
        method:'post',
        data
    })
}