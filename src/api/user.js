import request from '@/utils/request'

// 登录
export function login(data){
    return request({
        url:'/api/login',
        method:'post',
        data
    })
}

// 注销
export function logout(headers){
    return request({
        url:'/api/logout',
        method:'post',
        headers
    })
}

// 注册
export function register(data){
    return request({
        url:'/api/register',
        method:'post',
        data
    })
}

// 修改密码
export function resetPassword(data,headers){
    return request({
        url:'/api/resetPassword',
        method:'post',
        data,
        headers
    })
}

// 找回密码
export function retrievePassword(data){
    return request({
        url:'/api/retrievePassword',
        method:'post',
        data
    })
}