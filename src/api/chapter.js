import request from '@/utils/request'

// 章节详情
export function getChapterDetail(data){
    return request({
        url:'/api/books/detail',
        method:'post',
        data
    })
}

// 章节列表（分页）
export function getChapterList(data){
    return request({
        url:'/api/books/chapterList',
        method:'post',
        data
    })
}

// 获取章节列表（所有）
export function getChapterAllList(data){
    return request({
        url:'/api/books/chapterAllList',
        method:'post',
        data
    })
}